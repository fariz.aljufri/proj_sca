﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class CompanyViewModel
    {
        public int id { get; set; }

        public string code { get; set; }

        public string name { get; set; }

        public string address { get; set; }

        public string phone { get; set; }

        public string email { get; set; }

        public string created_by { get; set; }

        public DateTime created_date { get; set; }

        public string updated_by { get; set; }

        public DateTime? updated_date { get; set; }
        
        public bool is_delete { get; set; }
    }
}
