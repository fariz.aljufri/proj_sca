﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class EmployeeViewModel
    {

        public int id { get; set; }

        public string employee_number { get; set; }

        public string first_name { get; set; }

        public string last_name { get; set; }

        public int? m_company_id { get; set; }

        public string company_name { get; set; }

        public string email { get; set; }

        public string fullName
        {
            get
            {
                return first_name + " " + last_name;
            }
        }

        public string created_by { get; set; }

        public DateTime created_date { get; set; }

        public string updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public bool is_delete { get; set; }
    }
}
