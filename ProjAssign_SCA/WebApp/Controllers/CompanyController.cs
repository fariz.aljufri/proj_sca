﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class CompanyController : Controller
    {
        private readonly ILogger<CompanyController> _logger;
        private readonly CompanyService companyServ;

        public CompanyController(ILogger<CompanyController> logger, IConfiguration configuration)
        {
            _logger = logger;
            companyServ = new CompanyService(configuration);
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            //List<CompanyViewModel> list = await companyServ.GetAll();
            ResponseResult list = await companyServ.GetAll();
            return PartialView("_List", list.Entity);
        }

        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(CompanyViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await companyServ.Create(model);
                if (result.Success)
                {
                    ViewBag.Title = "Menambah";
                    ViewBag.Body = "Ditambah";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult result = await companyServ.GetById(id);
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CompanyViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await companyServ.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Mengubah";
                    ViewBag.Body = "Diubah";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Edit", model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await companyServ.GetById(id);
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(CompanyViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await companyServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Menghapus";
                    ViewBag.Body = "Dihapus";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Delete", model);
        }
    }
}
