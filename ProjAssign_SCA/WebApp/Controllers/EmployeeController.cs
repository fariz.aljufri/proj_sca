﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly ILogger<EmployeeController> _logger;
        private readonly EmployeeService employeeServ;

        public EmployeeController(ILogger<EmployeeController> logger, IConfiguration configuration)
        {
            _logger = logger;
            employeeServ = new EmployeeService(configuration);
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            //List<EmployeeViewModel> list = await employeeServ.GetAll();
            ResponseResult list = await employeeServ.GetAll();
            return PartialView("_List", list.Entity);
        }

        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(EmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await employeeServ.Create(model);
                if (result.Success)
                {
                    ViewBag.Title = "Menambah";
                    ViewBag.Body = "Ditambah";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult result = await employeeServ.GetById(id);
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await employeeServ.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Mengubah";
                    ViewBag.Body = "Diubah";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Edit", model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await employeeServ.GetById(id);
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(EmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await employeeServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Menghapus";
                    ViewBag.Body = "Dihapus";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Delete", model);
        }
    }
}
