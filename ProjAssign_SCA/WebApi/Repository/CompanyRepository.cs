﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;
using WebApi.Repositories;

namespace WebApi.Repository
{
    public class CompanyRepository : IRepository<CompanyViewModel>
    {
        private ProjDbContext _ProjDbContext = new ProjDbContext();
        private ResponseResult result = new ResponseResult();
        private string _username;

        public CompanyRepository()
        {
            _username = "Administrator";
        }

        public CompanyRepository(string username)
        {
            _username = username;
        }

        public ResponseResult Create(CompanyViewModel entity)
        {
            try
            {
                M_company item = new M_company();
                item.code = entity.code;
                item.name = entity.name;
                item.address = entity.address;
                item.phone = entity.phone;
                item.email = entity.email;

                item.created_by = _username;
                item.created_date = DateTime.Now;

                _ProjDbContext.m_company.Add(item);
                _ProjDbContext.SaveChanges();
                result.Entity = item;
            }
            catch (Exception x)
            {
                result.Entity = entity;
                result.Message = x.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(CompanyViewModel entity)
        {
            try
            {
                M_company item = _ProjDbContext.m_company
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;

                    result.Entity = item;
                    _ProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Item Tidak Ditemukan!";
                    result.Entity = entity;
                }
            }
            catch (Exception x)
            {
                result.Message = x.Message;
                result.Success = false;
            }
            return result;
        }

        public List<CompanyViewModel> GetAll()
        {
            List<CompanyViewModel> result = new List<CompanyViewModel>();
            try
            {
                result = (from o in _ProjDbContext.m_company
                          .Where(o => o.is_delete != true)
                          select new CompanyViewModel
                          {
                              id = o.id,
                              code = o.code,
                              name = o.name,
                              created_date = o.created_date,
                              created_by = o.created_by
                          }).ToList();
            }
            catch (Exception x)
            {
                string error = x.Message;
            }
            return result;
        }

        public CompanyViewModel GetById(long id)
        {
            CompanyViewModel result = new CompanyViewModel();
            try
            {
                result = (from o in _ProjDbContext.m_company
                          .Where(o => o.id == id && o.is_delete != true)
                          select new CompanyViewModel
                          {
                              id = o.id,
                              code = o.code,
                              name = o.name,
                              created_date = o.created_date,
                              created_by = o.created_by
                          }).FirstOrDefault();
            }
            catch (Exception x)
            {
                string error = x.Message;
            }
            return result;
        }

        public ResponseResult Update(CompanyViewModel entity)
        {
            try
            {
                M_company item = _ProjDbContext.m_company
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();

                if (item != null)
                {
                    item.code = entity.code;
                    item.name = entity.name;
                    item.address = entity.address;
                    item.phone = entity.phone;
                    item.email = entity.email;

                    item.updated_by = _username;
                    item.updated_date = DateTime.Now;

                    _ProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Item Tidak Ditemukan!";
                    result.Entity = entity;
                }
            }
            catch (Exception x)
            {
                result.Message = x.Message;
                result.Success = false;
            }
            return result;
        }
    }
}
