﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;
using WebApi.Repositories;

namespace WebApi.Repository
{
    public class EmployeeRepository : IRepository<EmployeeViewModel>
    {
        private ProjDbContext _ProjDbContext = new ProjDbContext();
        private ResponseResult result = new ResponseResult();
        private string _username;

        public EmployeeRepository()
        {
            _username = "Administrator";
        }

        public EmployeeRepository(string username)
        {
            _username = username;
        }

        public ResponseResult Create(EmployeeViewModel entity)
        {
            try
            {
                M_employee item = new M_employee();
                item.employee_number = entity.employee_number;
                item.first_name = entity.first_name;
                item.last_name = entity.last_name;
                item.m_company_id = entity.m_company_id;
                item.email = entity.email;

                item.created_by = _username;
                item.created_date = DateTime.Now;

                _ProjDbContext.m_employee.Add(item);
                _ProjDbContext.SaveChanges();
                result.Entity = item;
            }
            catch (Exception x)
            {
                result.Entity = entity;
                result.Message = x.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(EmployeeViewModel entity)
        {
            try
            {
                M_employee item = _ProjDbContext.m_employee
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;

                    result.Entity = item;
                    _ProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Item Tidak Ditemukan!";
                    result.Entity = entity;
                }
            }
            catch (Exception x)
            {
                result.Message = x.Message;
                result.Success = false;
            }
            return result;
        }

        public List<EmployeeViewModel> GetAll()
        {
            List<EmployeeViewModel> result = new List<EmployeeViewModel>();
            try
            {
                result = (from o in _ProjDbContext.m_employee
                          .Where(o => o.is_delete != true)
                          select new EmployeeViewModel
                          {
                              id = o.id,
                              employee_number = o.employee_number,
                              first_name = o.first_name,
                              last_name = o.last_name,
                              company_name = o.M_company.name,
                              created_date = o.created_date,
                              created_by = o.created_by
                          }).ToList();
            }
            catch (Exception x)
            {
                string error = x.Message;
            }
            return result;
        }

        public EmployeeViewModel GetById(long id)
        {
            EmployeeViewModel result = new EmployeeViewModel();
            try
            {
                result = (from o in _ProjDbContext.m_employee
                          .Where(o => o.id == id && o.is_delete != true)
                          select new EmployeeViewModel
                          {
                              id = o.id,
                              employee_number = o.employee_number,
                              first_name = o.first_name,
                              last_name = o.last_name,
                              company_name = o.M_company.name,
                              created_date = o.created_date,
                              created_by = o.created_by
                          }).FirstOrDefault();
            }
            catch (Exception x)
            {
                string error = x.Message;
            }
            return result;
        }

        public ResponseResult Update(EmployeeViewModel entity)
        {
            try
            {
                M_employee item = _ProjDbContext.m_employee
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();

                if (item != null)
                {
                    item.employee_number = entity.employee_number;
                    item.first_name = entity.first_name;
                    item.last_name = entity.last_name;
                    item.m_company_id = entity.m_company_id;
                    item.email = entity.email;

                    item.updated_by = _username;
                    item.updated_date = DateTime.Now;

                    _ProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Item Tidak Ditemukan!";
                    result.Entity = entity;
                }
            }
            catch (Exception x)
            {
                result.Message = x.Message;
                result.Success = false;
            }
            return result;
        }
    }
}
