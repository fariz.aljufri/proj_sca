﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_employee : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), MaxLength(11)]
        public int id { get; set; }

        [Required, MaxLength(50)]
        public string employee_number { get; set; }

        [Required, MaxLength(50)]
        public string first_name { get; set; }

        [MaxLength(50)]
        public string last_name { get; set; }

        [MaxLength(11)]
        public int? m_company_id { get; set; }

        [MaxLength(150)]
        public string email { get; set; }

        [ForeignKey("m_company_id")]
        public virtual M_company M_company { get; set; }
    }
}
