﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_company : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), MaxLength(11)]
        public int id { get; set; }

        [Required, MaxLength(50)]
        public string code { get; set; }

        [Required, MaxLength(50)]
        public string name { get; set; }

        [MaxLength(255)]
        public string address { get; set; }

        [MaxLength(50)]
        public string phone { get; set; }

        [MaxLength(50)]
        public string email { get; set; }
    }
}
