﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class BaseProperties
    {
        [Required, MaxLength(50)]
        public string created_by { get; set; }

        public DateTime created_date { get; set; }

        [MaxLength(50)]
        public string updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        [Required]
        public bool is_delete { get; set; }
    }
}
