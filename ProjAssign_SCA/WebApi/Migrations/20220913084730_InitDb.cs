﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class InitDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "m_company",
                columns: table => new
                {
                    id = table.Column<int>(maxLength: 11, nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<string>(maxLength: 50, nullable: false),
                    created_date = table.Column<DateTime>(nullable: false),
                    updated_by = table.Column<string>(maxLength: 50, nullable: true),
                    updated_date = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    code = table.Column<string>(maxLength: 50, nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: false),
                    address = table.Column<string>(maxLength: 255, nullable: true),
                    phone = table.Column<string>(maxLength: 50, nullable: true),
                    email = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_company", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_employee",
                columns: table => new
                {
                    id = table.Column<int>(maxLength: 11, nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<string>(maxLength: 50, nullable: false),
                    created_date = table.Column<DateTime>(nullable: false),
                    updated_by = table.Column<string>(maxLength: 50, nullable: true),
                    updated_date = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    employee_number = table.Column<string>(maxLength: 50, nullable: false),
                    first_name = table.Column<string>(maxLength: 50, nullable: false),
                    last_name = table.Column<string>(maxLength: 50, nullable: true),
                    m_company_id = table.Column<int>(maxLength: 11, nullable: true),
                    email = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_employee", x => x.id);
                    table.ForeignKey(
                        name: "FK_m_employee_m_company_m_company_id",
                        column: x => x.m_company_id,
                        principalTable: "m_company",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_m_employee_m_company_id",
                table: "m_employee",
                column: "m_company_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "m_employee");

            migrationBuilder.DropTable(
                name: "m_company");
        }
    }
}
