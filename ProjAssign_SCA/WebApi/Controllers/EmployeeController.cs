﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApi.DataModels;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using WebApi.Repository;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private EmployeeRepository employeeRepo = new EmployeeRepository();

        [HttpGet]
        public async Task<List<EmployeeViewModel>> Get()
        {
            return employeeRepo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<EmployeeViewModel> Get(long id)
        {
            return employeeRepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(EmployeeViewModel model)
        {
            return employeeRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(EmployeeViewModel model)
        {
            return employeeRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(int id)
        {
            EmployeeViewModel model = new EmployeeViewModel() { id = id };
            return employeeRepo.Delete(model);
        }
    }
}
