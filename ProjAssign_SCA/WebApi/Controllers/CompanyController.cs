﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApi.DataModels;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using WebApi.Repository;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private CompanyRepository companyRepo = new CompanyRepository();

        [HttpGet]
        public async Task<List<CompanyViewModel>> Get()
        {
            return companyRepo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<CompanyViewModel> Get(long id)
        {
            return companyRepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(CompanyViewModel model)
        {
            return companyRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(CompanyViewModel model)
        {
            return companyRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(int id)
        {
            CompanyViewModel model = new CompanyViewModel() { id = id };
            return companyRepo.Delete(model);
        }
    }
}
