﻿using System;

namespace Logic
{
    internal class Program
    {
        static void Main(string[] args)
        {
            new Program();
        }

        public Program()
        {
            bool keluar = false;
            while (!keluar)
            {
                try
                {
                    Console.WriteLine("=====  Project Assignment  =====");
                    Console.WriteLine(" 1. Algoritma Kelipatan 3 dan 4");
                    Console.WriteLine(" 2. Algoritma Piramida a");
                    Console.WriteLine(" 3. Algoritma Piramida b");
                    Console.WriteLine(" 4. Algoritma Piramida c");
                    Console.WriteLine(" 5. Algoritma Piramida d");
                    Console.WriteLine(" 6. Algoritma Mengubah Karakter");
                    Console.WriteLine();
                    Console.WriteLine(" 7. Keluar");
                    Console.WriteLine("================================");
                    Console.WriteLine();
                    Console.Write("Pilih soal : ");
                    int soal = int.Parse(Console.ReadLine());
                    switch (soal)
                    {
                        case 1:
                            Console.Clear();
                            Kelipatan kelipatan = new Kelipatan();
                            break;
                        case 2:
                            Console.Clear();
                            PiramidaA piramidaA = new PiramidaA();
                            break;
                        case 3:
                            PiramidaB piramidaB = new PiramidaB();
                            break;
                        case 4:
                            PiramidaC piramidaC = new PiramidaC();
                            break;
                        case 5:
                            PiramidaD piramidaD = new PiramidaD();
                            break;
                        case 6:
                            Karakter karakter = new Karakter();
                            break;
                        case 7:
                            break;
                        default:
                            Console.WriteLine("Tidak ada dalam pilihan");
                            break;
                    }
                    Console.WriteLine();
                    Console.Write("Apakah Ingin Keluar? [Y/T] : ");
                    keluar = Console.ReadLine().ToUpper() != "T";
                    Console.Clear();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine();
                }
            }
        }
    }
}
