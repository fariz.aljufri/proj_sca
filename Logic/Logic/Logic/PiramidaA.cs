﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic
{
    public class PiramidaA
    {
        public PiramidaA()
        {
            Console.WriteLine("=== Piramida A ===");
            Console.Write("Input nilai n : ");
            int n = int.Parse(Console.ReadLine());
            int angka = 1;

            for (int r = 1; r <= n; r++)
            {
                for (int c = 1; c <= r; c++)
                {
                    Console.Write($"{angka} ");
                }
                Console.Write("\n");
                angka++;
            }
        }
    }
}