﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic
{
    public class PiramidaB
    {
        public PiramidaB()
        {
            Console.Clear();
            Console.WriteLine("=== Piramida B ===");
            Console.Write("Input nilai n : ");
            int n = int.Parse(Console.ReadLine());
            int angka = 1;

            for (int r = 1; r <= n; r++)
            {
                int temp = angka;
                for (int c = 1; c <= r; c++)
                {
                    Console.Write($"{temp} ");
                    temp--;
                }
                Console.Write("\n");
                angka++;
            }
        }
    }
}
