﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic
{
    public class PiramidaC
    {
        public PiramidaC()
        {
            Console.Clear();
            Console.WriteLine("=== Piramida C ===");
            Console.Write("Input nilai n : ");
            int n = int.Parse(Console.ReadLine());
            int angka = 1;
            int temp = angka;

            for (int r = 1; r <= n; r++)
            {
                if (temp < n)
                {
                    for (int c = 1; c <= r; c++)
                    {
                        Console.Write($"{temp}\t");
                        if (temp >= n)
                            temp--;
                        else
                            temp++;
                    }
                }
                else
                {
                    for (int c = 1; c <= r; c++)
                    {
                        Console.Write($"{temp}\t");
                        if (temp > n)
                            temp++;
                        else
                            temp--;
                    }
                }
                Console.Write("\n");
                angka++;
            }
        }
    }
}