﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic
{
    public class PiramidaD
    {
        public PiramidaD()
        {
            Console.Clear();
            Console.WriteLine("=== Piramida D ===");
            Console.Write("Input nilai n : ");
            int n = int.Parse(Console.ReadLine());
            int angka = 1;

            for (int r = 0; r < n; r++)
            {
                if (angka < n)
                {
                    for (int c = 0; c < n; c++)
                    {
                        Console.WriteLine($"{angka}\t");
                        angka++;
                    }
                }
                else
                {
                    for (int c = 0; c < n; c++)
                    {
                        Console.WriteLine($"{angka}\t");
                        angka--;
                    }
                }
                //Console.WriteLine();
            }
        }
    }
}
