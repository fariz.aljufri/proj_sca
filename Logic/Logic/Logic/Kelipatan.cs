﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic
{
    public class Kelipatan
    {
        public Kelipatan()
        {
            Console.WriteLine("=== Kelipatan 3 dan 4 ===");
            Console.Write("Input nilai n : ");
            int n = int.Parse(Console.ReadLine());
            int angka = 1;
            string kel3 = "OK";
            string kel4 = "YES";

            for (int i = 0; i < n; i++)
            {
                if (angka % 3 == 0 && angka % 4 == 0)
                    Console.Write($"{"OKYES"} ");
                else if (angka % 3 == 0)
                    Console.Write("{0} ", kel3);
                else if (angka % 4 == 0)
                    Console.Write("{0} ", kel4);
                else
                    Console.Write("{0} ", angka);
                angka++;
            }
        }
    }
}
