﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic
{
    public class Karakter
    {
        public Karakter()
        {
            Console.Clear();
            Console.WriteLine("=== Karakter R/r ===");
            Console.Write("Input n : ");
            string n = Console.ReadLine();
            char[] huruf = n.ToCharArray();

            for (int i = 0; i < n.Length; i++)
            {
                if (huruf[i] == 'r')
                {
                    huruf[i] = 'l';
                }
                if (huruf[i] == 'R')
                {
                    huruf[i] = 'L';
                }
                Console.Write(huruf[i]);
            }
        }
    }
}
